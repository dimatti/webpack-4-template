const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const copyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env, argv) => {
	console.log(argv.mode);
	var jsOutputName, cssOutputName;
	if (argv.mode == 'development'){
		jsOutputName = './js/app.js';
		cssOutputName = './css/app.css';
	} else {
		jsOutputName ='./js/app.min.js';
		cssOutputName = './css/app.min.css';
	};

	return {
		context: path.resolve(__dirname, 'src'),
		entry: {
			js: './js/main.js',
		},
		output: {
			filename: jsOutputName,
		},
		module: {
			rules: [
				{
					test: /\.js$/,
					loader: 'babel-loader',
					exclude: '/node_modules/',
				},
				{
					test: /\.scss$/,
					use: [
						{
							loader: MiniCssExtractPlugin.loader,
							options: {
							},
						},
						{
							loader: 'css-loader',
							options: {
								url: false,
							},
						},
						'postcss-loader',
						'resolve-url-loader',
						'sass-loader',
					],
				},
				{
	        test: /\.(png|jpg|gif|svg)$/,
	        loader: 'file-loader',
	        options: {
	          name: '[path][name].[ext]?[hash]',
	        },
	      },
	      {
	      	test: /\.(woff|woff2|eot|ttf|otf)$/,
	      	/*use: [
	      		{
	      			loader: 'file-loader',
	      			options: {
	      				name: '[path][name].[ext]',
	      			},
	      		},
	      	],*/
	        loader: 'file-loader',
	        options: {
	          name: '[path][name].[ext]',
	        },
	      },
			],
		},
		devServer: {
			overlay: true,
			hot: true,
		},
		plugins: [
			new MiniCssExtractPlugin({
				filename: cssOutputName,
				//filename: path.resolve(__dirname, 'dist/css/app.css'),
				//chunkFilename: 'chunk.css',
				//allChunks: true,
			}),
			new copyWebpackPlugin([
					{
						from: './img',
						to: './img',
					},
					{
						from: './fonts',
						to: './fonts',
					},
				]),
		],
	  optimization: {
		  minimizer: [
		    new UglifyJsPlugin({
		      cache: true,
		      parallel: true,
		      sourceMap: false, // set to true if you want JS source maps
		    }),
		    new OptimizeCSSAssetsPlugin({})
		  ],
		},
	}
}