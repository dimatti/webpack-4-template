import $ from 'jquery';
import development from './development.js';
/*import carousel from './carousel.js';*/
/*import smoothScroll from './smoothScroll.js';*/
//import popper from 'popper.js';
//import bootstrap from 'bootstrap';

$(document).ready(function(){
	development.addWindowStatsElement();
	/*carousel.slickSliderInit();*/
	smoothScroll();
});

/* styles */
require('../scss/main.scss');
