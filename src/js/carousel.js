import $ from 'jquery';
import slick from 'slick-carousel';

let carousel = {
	slickSliderInit: function(){
		$('.js-slick-slider').slick({
			slidesToScroll: 1,
			arrows: true,
			slidesToShow: 6,
			prevArrow: $('.js-prev-arrow'),
			nextArrow: $('.js-next-arrow'),
			responsive: [
				{
					breakpoint: 1280,
					settings: {
						slidesToShow: 5,
					},
				},
				{
					breakpoint: 992,
					settings: "unslick",
				},
			],
			//centerMode: true,
			//centerPadding: '20px',
			// responsive: [
		 //    {
		 //      breakpoint: 1600,
		 //      settings: {
		 //        arrows: false,
		 //        slidesToShow: 2,
		 //      },
		 //    },
		 //    {
		 //      breakpoint: 1280,
		 //      settings: "unslick",
		 //    },
	  // 	],
		});
	},
};

export default carousel;