import $ from 'jquery';

let development = {
	addWindowStatsElement: function(){
		let style = {
			"background-color": "rgba(0,0,0,0.6)",
			"color": "#ffffff",
			"position": "fixed",
			"font-size": "12px",
			"font-family": "monospace",
			"padding": "0 8px",
			"z-index": "1000000",
		};
		let windowStatsElement = $('<div></div>');
		windowStatsElement.css(style);
		$('body').prepend(windowStatsElement);

		let refreshWindowStats = function(){
			let viewPortWidth = document.documentElement.clientWidth;
			let viewPortHeight = document.documentElement.clientHeight;
			let windowWidth = window.innerWidth;
			let windowHeight = window.innerHeight;
			let template = `<div>Viewport: `+viewPortWidth+`x`+viewPortHeight+` | `+`Window: `+windowWidth+`x`+windowHeight+`</div>`;
			windowStatsElement.html(template);
		};
		refreshWindowStats();
		window.onresize = refreshWindowStats;
	},
};


export default development;