import $ from 'jquery';

let smoothScroll = function(){

	var $page = $('html, body');
	$('a[href*="#"]').click(function() {
    $page.animate({
      scrollTop: $($.attr(this, 'href')).offset().top
    }, 400);
    return false;
	});
};

export default smoothScroll;